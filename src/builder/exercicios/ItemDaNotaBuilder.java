package builder.exercicios;

import builder.ItemDaNota;

public class ItemDaNotaBuilder {

    private String nome;
    private double valor;

    ItemDaNotaBuilder comNome(String nome) {
        this.nome = nome;
        return this;
    }

    ItemDaNotaBuilder comValor(double valor) {
        this.valor = valor;
        return this;
    }

    ItemDaNota build() {
        return new ItemDaNota(nome, valor);
    }

}
