package builder;

public class TesteDaNotaFiscal {

    public static void main(String[] args) {

        NotaFiscalBuilder builder = new NotaFiscalBuilder();
        builder.paraEmpresa("Caelum Ensino e Invação")
                .comCNPJ("12.345.678/0001-12")
                .com(new ItemDaNota("Item 1", 200.00))
                .com(new ItemDaNota("Item 2", 300.00))
                .com(new ItemDaNota("Item 3", 400.00))
                .comObservacoes("Observação qualquer");

        NotaFiscal notaFiscal = builder.constroi();
        System.out.println(notaFiscal.getValorBruto());
    }
}
