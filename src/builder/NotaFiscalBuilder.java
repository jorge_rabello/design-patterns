package builder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class NotaFiscalBuilder {

    List<ItemDaNota> todosItems = new ArrayList<>();
    private String razaoSocial;
    private String cnpj;
    private double valorBruto;
    private double impostos;
    private String observacoes;
    private Calendar data;

    public NotaFiscalBuilder() {
        this.data = Calendar.getInstance();
    }

    NotaFiscalBuilder paraEmpresa(String razaoSocial) {
        this.razaoSocial = razaoSocial;
        return this;
    }

    NotaFiscalBuilder comCNPJ(String cnpj) {
        this.cnpj = cnpj;
        return this;
    }

    NotaFiscalBuilder com(ItemDaNota item) {
        todosItems.add(item);
        valorBruto += item.getValor();
        impostos += item.getValor() * 0.5;
        return this;
    }

    NotaFiscalBuilder comObservacoes(String observacoes) {
        this.observacoes = observacoes;
        return this;
    }

    NotaFiscalBuilder naData(Calendar data) {
        this.data = data;
        return this;
    }

    NotaFiscal constroi() {
        return new NotaFiscal(razaoSocial, cnpj, data, valorBruto, impostos, todosItems, observacoes);
    }

}
