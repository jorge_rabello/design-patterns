package state;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import decorator.Item;

public class Orcamento {

    private final List<Item> items;
    protected double valor;

    protected EstadoDeUmOrcamento estadoAtual;

    public Orcamento(double valor) {

        this.valor = valor;
        this.items = new ArrayList<>();
        estadoAtual = new EmAprovacao();
    }

    public double getValor() {
        return valor;
    }

    public void adicionaItem(Item item) {
        this.items.add(item);
    }

    public List<Item> getItems() {
        return Collections.unmodifiableList(items);
    }

    public void aplicaDescontoExtra() {
        estadoAtual.aplicaDescontoExtra(this);
    }

    public void aprova() {
        estadoAtual.aprova(this);
    }

    public void reprova() {
        estadoAtual.reprova(this);
    }

    public void finaliza() {
        estadoAtual.finaliza(this);
    }
}
