package state.exercicios;

public class TesteConta {

    public static void main(String[] args) {
        Conta contaDoJorge = new Conta("Jorge", 100.0);
        Conta contaDoAbreu = new Conta("Abreu", 100000.0);

        System.out.println("Saldo de: " + contaDoJorge.getTitular() + contaDoJorge.saldo);
        System.out.println("Saldo de: " + contaDoAbreu.getTitular() + contaDoAbreu.saldo);

        contaDoJorge.saca(200.00);
        contaDoAbreu.saca(200.00);

        System.out.println("Saldo de: " + contaDoJorge.getTitular() + contaDoJorge.saldo);
        System.out.println("Saldo de: " + contaDoAbreu.getTitular() + contaDoAbreu.saldo);

        contaDoJorge.deposita(10.00);
        contaDoAbreu.deposita(10.00);

        System.out.println("Saldo de: " + contaDoJorge.getTitular() + contaDoJorge.saldo);
        System.out.println("Saldo de: " + contaDoAbreu.getTitular() + contaDoAbreu.saldo);

        contaDoAbreu.saca(20.00);
        System.out.println("Saldo de: " + contaDoAbreu.getTitular() + contaDoAbreu.saldo);
        contaDoJorge.saca(20.00);
        System.out.println("Saldo de: " + contaDoJorge.getTitular() + contaDoJorge.saldo);

    }
}
