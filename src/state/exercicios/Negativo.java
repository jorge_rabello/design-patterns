package state.exercicios;

public class Negativo implements EstadoDeUmaConta {
    @Override
    public void saca(Conta conta, Double valor) {
        throw new RuntimeException("Erro ao sacar - conta com valor negativo !");
    }

    @Override
    public void deposita(Conta conta, Double valor) {
        conta.saldo += valor * 0.95;
        if (conta.saldo >= 0) {
            conta.estadoAtual = new Positivo();
        }
    }

    @Override
    public void ficaPositivo(Conta conta) {
        conta.estadoAtual = new Positivo();
    }

    @Override
    public void ficaNegativo(Conta conta) {
        throw new RuntimeException("Conta já negativada !");
    }
}
