package state.exercicios;

public class Positivo implements EstadoDeUmaConta {

    @Override
    public void saca(Conta conta, Double valor) {
        conta.saldo -= valor;

        if (conta.saldo < 0) {
            conta.estadoAtual = new Negativo();
        }
    }

    @Override
    public void deposita(Conta destino, Double valor) {
        destino.saldo += valor * 0.98;
    }

    @Override
    public void ficaPositivo(Conta conta) {
        throw new RuntimeException("Conta já está ok");
    }

    @Override
    public void ficaNegativo(Conta conta) {
        conta.estadoAtual = new Negativo();
    }
}
