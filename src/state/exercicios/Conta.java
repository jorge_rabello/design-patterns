package state.exercicios;

public class Conta {

    protected EstadoDeUmaConta estadoAtual;
    protected Double saldo;
    private String titular;

    public Conta() {
        estadoAtual = new Positivo();
    }

    public Conta(String titular, Double saldo) {
        this.titular = titular;
        this.saldo = saldo;
        estadoAtual = new Positivo();
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public void deposita(Double valor) {
        estadoAtual.deposita(this, valor);
    }

    public void saca(Double valor) {
        estadoAtual.saca(this, valor);
    }

    public void ficaPositivo() {
        estadoAtual.ficaPositivo(this);
    }

    public void ficaNegativo() {
        estadoAtual.ficaNegativo(this);
    }
}
