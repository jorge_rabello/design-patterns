package state.exercicios;

public interface EstadoDeUmaConta {

    void saca(Conta conta, Double valor);

    void deposita(Conta conta, Double valor);

    void ficaPositivo(Conta conta);

    void ficaNegativo(Conta conta);
}
