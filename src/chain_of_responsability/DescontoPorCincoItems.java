package chain_of_responsability;

public class DescontoPorCincoItems implements Desconto {

    private Desconto proximo;

    @Override
    public void setProximo(Desconto proximo) {
        this.proximo = proximo;
    }

    @Override
    public double desconta(Orcamento orcamento) {
        // se tiver mais de 5 items, merece um desconto !
        if (orcamento.getItems().size() > 5) {
            return orcamento.getValor() * 0.1;
        } else {
            return proximo.desconta(orcamento);
        }
    }
}
