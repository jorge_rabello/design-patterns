package chain_of_responsability;

public interface Desconto {

    void setProximo(Desconto desconto);

    double desconta(Orcamento orcamento);
}
