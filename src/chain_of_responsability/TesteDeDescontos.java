package chain_of_responsability;

public class TesteDeDescontos {

    public static void main(String[] args) {

        CalculadorDeDesconto descontos = new CalculadorDeDesconto();

        Orcamento orcamento = new Orcamento(600.0);
        orcamento.adicionaItem(new Item("CANETA", 250.0));
        orcamento.adicionaItem(new Item("LÁPIS", 250.0));

        double descontoFinal = descontos.calcula(orcamento);

        System.out.println(descontoFinal);

        Item lapis = new Item("LAPIS", 15.00);
        Item caneta = new Item("CANETA", 15.00);
        Item boracha = new Item("BORRACHA", 15.00);


        Orcamento orcamentoNovo = new Orcamento(45.0);
        orcamentoNovo.adicionaItem(caneta);
        orcamentoNovo.adicionaItem(lapis);
        orcamentoNovo.adicionaItem(boracha);

        double descontoNovo = descontos.calcula(orcamentoNovo);
        System.out.println(descontoNovo);

    }

}
