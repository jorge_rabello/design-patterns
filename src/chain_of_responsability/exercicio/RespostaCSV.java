package chain_of_responsability.exercicio;

public class RespostaCSV implements Resposta {

    private Resposta outraResposta;

    public RespostaCSV(Resposta outraResposta) {
        this.outraResposta = outraResposta;
    }

    public RespostaCSV() {
        this.outraResposta = null;
    }

    @Override
    public String responde(Requisicao req, Conta conta) {
        if (req.getFormat() == Formato.CSV) {
            System.out.println(conta.getTitular() + "," + conta.getSaldo());
        } else if (outraResposta != null) {
            outraResposta.responde(req, conta);
        } else {
            throw new RuntimeException("Formato de resposta não encontrado");
        }
        return null;
    }

}
