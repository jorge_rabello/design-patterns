package chain_of_responsability.exercicio;

public class Requisicao {

    private Formato format;

    public Requisicao(Formato format) {
        this.format = format;
    }

    public Formato getFormat() {
        return format;
    }

}
