package chain_of_responsability.exercicio;

public class ChainDeRespostas {

    public String responde(Requisicao req, Conta conta) {

        Resposta r1 = new RespostaCSV(
                new RespostaXML(
                        new RespostaPorcento()));

        return r1.responde(req, conta);
    }

}
