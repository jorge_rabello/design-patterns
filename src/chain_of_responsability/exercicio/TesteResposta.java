package chain_of_responsability.exercicio;

public class TesteResposta {
    public static void main(String[] args) {

        System.out.println("\nTESTANDO O SUCESSO:...");
        Requisicao requisicao = new Requisicao(Formato.XML);
        ChainDeRespostas chainDeRespostas = new ChainDeRespostas();
        chainDeRespostas.responde(requisicao, new Conta("Jorge", 500.0));


        System.out.println("\nTESTANDO O ERRO:...");
        Requisicao requisicaoErrada = new Requisicao(Formato.XPTO);
        ChainDeRespostas respostaComErro = new ChainDeRespostas();
        respostaComErro.responde(requisicaoErrada, new Conta("Jorge", 500.0));

    }
}
