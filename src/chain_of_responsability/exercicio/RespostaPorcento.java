package chain_of_responsability.exercicio;

public class RespostaPorcento implements Resposta {

    private Resposta outraResposta;

    public RespostaPorcento(Resposta outraResposta) {
        this.outraResposta = outraResposta;
    }

    public RespostaPorcento() {
        this.outraResposta = null;
    }

    @Override
    public String responde(Requisicao req, Conta conta) {
        if (req.getFormat() == Formato.PORCENTO) {
            System.out.println(conta.getTitular() + "%" + conta.getSaldo());
        } else if (outraResposta != null) {
            outraResposta.responde(req, conta);
        } else {
            throw new RuntimeException("Formato de resposta não encontrado");
        }
        return null;
    }

}
