package chain_of_responsability.exercicio;

public interface Resposta {

    String responde(Requisicao req, Conta conta);

}
