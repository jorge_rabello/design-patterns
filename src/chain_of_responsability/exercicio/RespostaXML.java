package chain_of_responsability.exercicio;

public class RespostaXML implements Resposta {

    private Resposta outraResposta;

    public RespostaXML(Resposta outraResposta) {
        this.outraResposta = outraResposta;
    }

    public RespostaXML() {
        this.outraResposta = null;
    }

    @Override
    public String responde(Requisicao req, Conta conta) {
        if (req.getFormat() == Formato.XML) {
            System.out.println("<conta><titular>"
                    + conta.getTitular()
                    + "</titular><saldo>"
                    + conta.getSaldo()
                    + "</saldo></conta>");
        } else if (outraResposta != null) {
            outraResposta.responde(req, conta);
        } else {
            throw new RuntimeException("Formato de resposta não encontrado");
        }
        return null;
    }

}
