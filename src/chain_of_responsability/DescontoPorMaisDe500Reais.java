package chain_of_responsability;

public class DescontoPorMaisDe500Reais implements Desconto {

    private Desconto proximo;

    @Override
    public void setProximo(Desconto proximo) {
        this.proximo = proximo;
    }

    @Override
    public double desconta(Orcamento orcamento) {
        // se o valor do orçamento ultrapassar 500 mitos, concede desconto
        if (orcamento.getValor() > 500.0) {
            return orcamento.getValor() * 0.07;
        } else {
            return proximo.desconta(orcamento);
        }
    }
}
