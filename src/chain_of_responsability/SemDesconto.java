package chain_of_responsability;

public class SemDesconto implements Desconto {

    @Override
    public void setProximo(Desconto desconto) {
        // não tem próximo
    }

    @Override
    public double desconta(Orcamento orcamento) {
        return 0;
    }

}
