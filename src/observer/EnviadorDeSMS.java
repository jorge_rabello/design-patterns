package observer;

public class EnviadorDeSMS implements AcaoAposGerarNota {

    @Override
    public void executa(NotaFiscal notaFiscal) {
        System.out.println("Enviei por SMS");
    }
}
