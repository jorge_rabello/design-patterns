package observer;

public class NotaFiscalDAO implements AcaoAposGerarNota {

    @Override
    public void executa(NotaFiscal notaFiscal) {
        System.out.println("Salvei no banco de dados");
    }
}
