package template_method.exercicios;

import java.util.ArrayList;
import java.util.List;

import template_method.Item;
import template_method.Orcamento;
import template_method.TemplateDeImpostCondicional;

/*
Crie o imposto IHIT, que tem a seguinte regra: caso existam 2 itens com o mesmo
nome, o imposto deve ser de 13% mais R$100,00.
Caso contrário, o valor do imposto deverá ser (1% * o número de ítens no orçamento).
 */
public class IHIT extends TemplateDeImpostCondicional {

    @Override
    public double minimaTaxaxao(Orcamento orcamento) {
        return orcamento.getValor() * (0.01 * orcamento.getItems().size());
    }

    @Override
    public double maximaTaxacao(Orcamento orcamento) {
        return orcamento.getValor() * 0.13 + 100;
    }

    @Override
    public boolean deveUsarMaximaTaxacao(Orcamento orcamento) {
        List<String> noOrcamento = new ArrayList<String>();

        for (Item item : orcamento.getItems()) {
            if (noOrcamento.contains(item.getNome())) {
                return true;
            } else {
                noOrcamento.add(item.getNome());
            }
        }
        return false;
    }
}
