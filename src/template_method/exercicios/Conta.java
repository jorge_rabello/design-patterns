package template_method.exercicios;

public class Conta {

    private String titular;
    private double saldo;
    private String nome;

    public Conta(String titular, double saldo, String nome) {
        this.titular = titular;
        this.saldo = saldo;
        this.nome = nome;
    }

    public void deposita(double valor) {
        this.saldo += valor;
    }

    public double getSaldo() {
        return saldo;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String geNome() {
        return this.nome;
    }
}
