package template_method.exercicios.relatorios;

import java.util.List;

import template_method.exercicios.Conta;

public class RelatorioSimples extends Relatorio {

    @Override
    protected void cabecalho() {
        System.out.println("Banco XYZ");
    }

    @Override
    protected void rodape() {
        System.out.println("(11) 1234-5678");
    }

    @Override
    protected void corpo(List<Conta> contas) {
        for (Conta conta : contas) {
            System.out.println(conta.geNome() + " - " + conta.getSaldo());
        }
    }
}
