package strategy.execicios;

import java.util.Random;

import strategy.execicios.contratos.Investimento;
import strategy.execicios.estrategias.Arrojado;
import strategy.execicios.estrategias.Conservador;
import strategy.execicios.estrategias.Moderado;
import strategy.execicios.modelos.Conta;

public class Main {

    public static void main(String[] args) {

        Random random = new Random(100);

        Investimento conservador = new Conservador();
        Investimento moderado = new Moderado();
        Investimento arrojado = new Arrojado(random);

        Conta conta = new Conta(1000);
        Conta conta2 = new Conta(1000);
        Conta conta3 = new Conta(1000);

        RealizadorDeInvestimentos realizadorDeInvestimentos = new RealizadorDeInvestimentos();

        realizadorDeInvestimentos.realizaInvestimento(conta, conservador);
        realizadorDeInvestimentos.realizaInvestimento(conta2, moderado);
        realizadorDeInvestimentos.realizaInvestimento(conta3, arrojado);
    }
}
