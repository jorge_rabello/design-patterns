package strategy.execicios.contratos;

import strategy.execicios.modelos.Conta;

public interface Investimento {

    double calcula(Conta conta);

}
