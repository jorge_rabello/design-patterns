package strategy.execicios.estrategias;

import java.util.Random;

import strategy.execicios.contratos.Investimento;
import strategy.execicios.modelos.Conta;

public class Moderado implements Investimento {

    private Random random;

    public Moderado() {
        this.random = new Random();
    }

    @Override
    public double calcula(Conta conta) {
        if (random.nextInt(2) == 0) {
            return conta.getSaldo() * 0.025;
        } else {
            return conta.getSaldo() * 0.007;
        }
    }
}
