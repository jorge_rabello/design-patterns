package strategy.execicios.estrategias;

import strategy.execicios.contratos.Investimento;
import strategy.execicios.modelos.Conta;

public class Conservador implements Investimento {

    @Override
    public double calcula(Conta conta) {
        return conta.getSaldo() * 0.008;
    }
}
