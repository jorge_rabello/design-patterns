package decorator;

public abstract class TemplateDeImpostCondicional extends Imposto {

    @Override
    public final double calcula(Orcamento orcamento) {

        if (deveUsarMaximaTaxacao(orcamento)) {
            return maximaTaxacao(orcamento) + calculoDoOutroImposto(orcamento);
        } else {
            return minimaTaxaxao(orcamento) + calculoDoOutroImposto(orcamento);
        }

    }

    public abstract double minimaTaxaxao(Orcamento orcamento);

    public abstract double maximaTaxacao(Orcamento orcamento);

    public abstract boolean deveUsarMaximaTaxacao(Orcamento orcamento);
}
