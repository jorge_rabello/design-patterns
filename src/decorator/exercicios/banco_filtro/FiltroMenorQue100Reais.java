package decorator.exercicios.banco_filtro;

import java.util.ArrayList;
import java.util.List;

public class FiltroMenorQue100Reais extends Filtro {

    public FiltroMenorQue100Reais(Filtro outroFiltro) {
        super(outroFiltro);
    }

    public FiltroMenorQue100Reais() {
        super();
    }

    @Override
    public List<Conta> filtra(List<Conta> contas) {
        ArrayList<Conta> filtrada = new ArrayList<>();
        for (Conta c : contas) {
            if (c.getValor() < 100) {
                filtrada.add(c);
            }
        }
        filtrada.addAll(proximo(contas));
        return filtrada;
    }

}
