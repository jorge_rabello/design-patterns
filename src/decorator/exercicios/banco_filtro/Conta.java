package decorator.exercicios.banco_filtro;

import java.util.Calendar;

public class Conta {

    private String titular;
    private double saldo;
    private double valor;
    private Calendar dataAbertura;

    public Conta(String titular, double saldo) {
        this.titular = titular;
        this.saldo = saldo;
    }

    public void deposita(double valor) {
        this.saldo += valor;
    }

    public double getSaldo() {
        return saldo;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public double getValor() {
        return this.valor;
    }

    public Calendar getDataAbertura() {
        return this.dataAbertura;
    }
}
