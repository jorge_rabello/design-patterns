package decorator.exercicios.banco_filtro;

import java.util.ArrayList;
import java.util.List;

public class FiltroMaiorQue500MilReais extends Filtro {

    public FiltroMaiorQue500MilReais(Filtro outroFiltro) {
        super(outroFiltro);
    }

    public FiltroMaiorQue500MilReais() {
        super();
    }

    @Override
    public List<Conta> filtra(List<Conta> contas) {
        ArrayList<Conta> filtrada = new ArrayList<>();
        for (Conta c : contas) {
            if (c.getValor() > 500000) {
                filtrada.add(c);
            }
        }
        filtrada.addAll(proximo(contas));
        return filtrada;
    }
}
