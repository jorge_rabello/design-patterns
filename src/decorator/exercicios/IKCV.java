package decorator.exercicios;

import decorator.Imposto;
import decorator.Orcamento;

public class IKCV extends Imposto {

    public IKCV(Imposto outroImposto) {
        super(outroImposto);
    }

    public IKCV() {
        super();
    }

    @Override
    public double calcula(Orcamento orcamento) {
        return orcamento.getValor() * 0.06 + calculoDoOutroImposto(orcamento);
    }
}
