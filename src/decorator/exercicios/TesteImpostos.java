package decorator.exercicios;

import decorator.ICMS;
import decorator.Orcamento;

public class TesteImpostos {
    public static void main(String[] args) {
        ImpostoMuitoAlto impostos = new ImpostoMuitoAlto(new ICMS());

        Orcamento orcamento = new Orcamento(500);

        System.out.println(impostos.calcula(orcamento));
    }
}
