package decorator;

public class TesteDeImpostosComplexos {

    public static void main(String[] args) {
        ISS iss = new ISS(new ICMS(new ICPP()));

        Orcamento orcamento = new Orcamento(500);

        double valor = iss.calcula(orcamento);

        System.out.println(valor);
    }
}
